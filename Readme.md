This project is for the case https://hibernate.atlassian.net/browse/HHH-15166 

1. Temurin 11: both Ok
```bash
cd docker 
JAVA_VERSION=maven:3.8.4-eclipse-temurin-11 doc up -V --abort-on-container-exit

# Output:
mvntest_1  | [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0
```

2. Temurin 16 or 17: ok with postgres, ko with H2

```bash
cd docker 
# May need several launch, because it depends on the clock
JAVA_VERSION=maven:3.8.4-eclipse-temurin-16 doc up -V --abort-on-container-exit
# or 
JAVA_VERSION=maven:3.8.4-eclipse-temurin-17 doc up -V --abort-on-container-exit

# Output:
mvntest_1  | 2022-04-04 15:59:48.004 DEBUG 119 --- [           main] o.s.orm.jpa.JpaTransactionManager        : Found thread-bound EntityManager [SessionImpl(1309912859<open>)] for JPA transaction
mvntest_1  | 2022-04-04 15:59:48.004 DEBUG 119 --- [           main] o.s.orm.jpa.JpaTransactionManager        : Participating in existing transaction
mvntest_1  | 2022-04-04 15:59:48.142 DEBUG 119 --- [           main] org.hibernate.SQL                        : call next value for test.seq_my_entity
mvntest_1  | 2022-04-04 15:59:48.268 DEBUG 119 --- [           main] org.hibernate.SQL                        : insert into test.my_entity (created_at, updated_at, name, id) values (?, ?, ?, ?)
mvntest_1  | 2022-04-04 15:59:48.281 TRACE 119 --- [           main] o.h.type.descriptor.sql.BasicBinder      : binding parameter [1] as [TIMESTAMP] - [2022-04-04T17:59:48.124896300+02:00[Europe/Zurich]]
mvntest_1  | 2022-04-04 15:59:48.282 TRACE 119 --- [           main] o.h.type.descriptor.sql.BasicBinder      : binding parameter [2] as [TIMESTAMP] - [2022-04-04T17:59:48.206273300+02:00[Europe/Zurich]]
mvntest_1  | 2022-04-04 15:59:48.284 TRACE 119 --- [           main] o.h.type.descriptor.sql.BasicBinder      : binding parameter [3] as [VARCHAR] - [Initial]
mvntest_1  | 2022-04-04 15:59:48.287 TRACE 119 --- [           main] o.h.type.descriptor.sql.BasicBinder      : binding parameter [4] as [BIGINT] - [1]
# Java uses nanoseconds
mvntest_1  | 2022-04-04 15:59:48.305  INFO 119 --- [           main] net.kernevez.hibernate.MyEntityH2Test    : Saved updateDate 2022-04-04T17:59:48.206273300+02:00[Europe/Zurich]
mvntest_1  | 2022-04-04 15:59:48.353 DEBUG 119 --- [           main] org.hibernate.SQL                        : select myentity0_.updated_at as col_0_0_ from test.my_entity myentity0_ where myentity0_.id=?
mvntest_1  | 2022-04-04 15:59:48.355 TRACE 119 --- [           main] o.h.type.descriptor.sql.BasicBinder      : binding parameter [1] as [BIGINT] - [1]
mvntest_1  | 2022-04-04 15:59:48.361 TRACE 119 --- [           main] o.h.type.descriptor.sql.BasicExtractor   : extracted value ([col_0_0_] : [TIMESTAMP]) - [2022-04-04T17:59:48.206273+02:00[Europe/Zurich]]
# Failure because value is truncated to milliseconds 
mvntest_1  | 2022-04-04 15:59:48.372  INFO 119 --- [           main] net.kernevez.hibernate.MyEntityH2Test    : Real updateDate 2022-04-04T17:59:48.206273+02:00[Europe/Zurich]
mvntest_1  | 2022-04-04 15:59:48.373 DEBUG 119 --- [           main] org.hibernate.SQL                        : update test.my_entity set created_at=?, updated_at=?, name=? where id=? and updated_at=?
mvntest_1  | 2022-04-04 15:59:48.374 TRACE 119 --- [           main] o.h.type.descriptor.sql.BasicBinder      : binding parameter [1] as [TIMESTAMP] - [2022-04-04T17:59:48.124896300+02:00[Europe/Zurich]]
mvntest_1  | 2022-04-04 15:59:48.374 TRACE 119 --- [           main] o.h.type.descriptor.sql.BasicBinder      : binding parameter [2] as [TIMESTAMP] - [2022-04-04T17:59:48.373040500+02:00[Europe/Zurich]]
mvntest_1  | 2022-04-04 15:59:48.374 TRACE 119 --- [           main] o.h.type.descriptor.sql.BasicBinder      : binding parameter [3] as [VARCHAR] - [New name]
mvntest_1  | 2022-04-04 15:59:48.374 TRACE 119 --- [           main] o.h.type.descriptor.sql.BasicBinder      : binding parameter [4] as [BIGINT] - [1]
mvntest_1  | 2022-04-04 15:59:48.375 TRACE 119 --- [           main] o.h.type.descriptor.sql.BasicBinder      : binding parameter [5] as [TIMESTAMP] - [2022-04-04T17:59:48.206273300+02:00[Europe/Zurich]]
mvntest_1  | 2022-04-04 15:59:48.378  INFO 119 --- [           main] o.h.e.j.b.internal.AbstractBatchImpl     : HHH000010: On release of batch it still contained JDBC statements
mvntest_1  | 2022-04-04 15:59:48.379 ERROR 119 --- [           main] o.h.e.jdbc.batch.internal.BatchingBatch  : HHH000315: Exception executing batch [org.hibernate.StaleStateException: Batch update returned unexpected row count from update [0]; actual row count: 0; expected: 1; statement executed: update test.my_entity set created_at=?, updated_at=?, name=? where id=? and updated_at=?], SQL: update test.my_entity set created_at=?, updated_at=?, name=? where id=? and updated_at=?
mvntest_1  | 2022-04-04 15:59:48.407 DEBUG 119 --- [           main] o.s.orm.jpa.JpaTransactionManager        : Initiating transaction rollback
mvntest_1  | 2022-04-04 15:59:48.407 DEBUG 119 --- [           main] o.s.orm.jpa.JpaTransactionManager        : Rolling back JPA transaction on EntityManager [SessionImpl(1309912859<open>)]
mvntest_1  | 2022-04-04 15:59:48.408 DEBUG 119 --- [           main] o.h.e.t.internal.TransactionImpl         : rolling back
mvntest_1  | 2022-04-04 15:59:48.412 DEBUG 119 --- [           main] o.s.orm.jpa.JpaTransactionManager        : Closing JPA EntityManager [SessionImpl(1309912859<open>)] after transaction
```

For H2, we can change the behaviour by using `timestamp(9)` instead of `datetime`, in this case postgres will fail as it only support 6 digits. 
```
<<<              - column:
<<<                  name: UPDATED_AT
<<<                  type: timestamp(9)
---------
>>>              - column:
>>>                  name: UPDATED_AT
>>>                  type: timestamp(9)
```



