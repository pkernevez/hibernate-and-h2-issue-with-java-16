package net.kernevez.hibernate.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.time.Clock;
import java.time.ZoneId;
import java.util.TimeZone;

@Configuration
@SuppressWarnings("unused")
public class TimeZoneConfig {

    public static final TimeZone TIME_ZONE = TimeZone.getTimeZone("Europe/Zurich");

    public static final ZoneId ZONE_ID = TIME_ZONE.toZoneId();

    @PostConstruct
    public void started() {
        TimeZone.setDefault(TIME_ZONE);
    }

    @Bean
    public Clock getClock() {
        return Clock.system(ZONE_ID);
    }
}
