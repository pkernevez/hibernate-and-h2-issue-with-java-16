package net.kernevez.hibernate.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.ZonedDateTime;
import java.util.List;

public interface MyEntityRepository extends JpaRepository<MyEntity, Long> {

    @Query("SELECT b.updatedAt FROM MyEntity b WHERE (b.id =:id)")
    ZonedDateTime getUpdatedAt(long id);
}
