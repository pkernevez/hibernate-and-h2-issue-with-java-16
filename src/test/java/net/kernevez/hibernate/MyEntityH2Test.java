package net.kernevez.hibernate;

import net.kernevez.hibernate.conf.JpaAuditingConfiguration;
import net.kernevez.hibernate.conf.TimeZoneConfig;
import net.kernevez.hibernate.entity.MyEntity;
import net.kernevez.hibernate.entity.MyEntityRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@ActiveProfiles("h2")
@Import({JpaAuditingConfiguration.class, TimeZoneConfig.class})
@Transactional
class MyEntityH2Test {
    private static Logger logger = LoggerFactory.getLogger(MyEntityH2Test.class);

    @Autowired
    private EntityManager em;

    @Autowired
    private MyEntityRepository repository;

    @Test
    void testUpdate_ShouldNotFailWithH2() {
        // Given a managed Entity
        MyEntity aChild = new MyEntity("Initial");
        repository.save(aChild);
        em.flush();

        logger.info("Saved updateDate {}",aChild.getUpdatedAt());
        logger.info("Real updateDate {}",repository.getUpdatedAt(aChild.getId()));

        // When
        aChild.setName("New name");
        em.flush();

        // Then
        // Only need to not receive an exception
    }

}